<?php
namespace Rubeus\IntegracaoLyceum;
use Rubeus\ContenerDependencia\Conteiner;

class ClienteWSPessoa extends ClienteWS{

    public function verificarPessoalyceum($cpf){
        $inicio = date('Y-m-d H:i:s');
        if($this->conectar(EnumURL::wsPessoa)){
            try{
                $dados = $this->clienteWS->ListarPessoas(array('cpf'=>$cpf));
                $fim = date('Y-m-d H:i:s');
                $this->registrarChamada('ListarPessoas', $inicio, $fim, $dados,$cpf);
                return $dados->listaPessoaDto->listaPessoaDto;
            }catch(\Exception $e){
                $this->setErro($e->getMessage(), 'ListarPessoas', $inicio, $fim, $cpf);
            }
        }
        return false;
    }

    public function verificarResponsavelFinanceiro($cpf){
        $inicio = date('Y-m-d H:i:s');
        if($this->conectar(EnumURL::wsPessoa)){
            try{
                $dados = $this->clienteWS->ListarResponsaveisFinanceiros(array('cpf'=>$cpf));
                $fim = date('Y-m-d H:i:s');
                $this->registrarChamada('ListarResponsaveisFinanceiros', $inicio, $fim, $dados,$cpf);
                return $dados->listaResponsaveisFinanceirosDto->listaResponsaveisFinanceirosDto;
            }catch(\Exception $e){
                $this->setErro($e->getMessage(), 'ListarResponsaveisFinanceiros', $inicio, $fim, $cpf);
            }
        }
        return false;
    }

    public function cadastrarPessoa($pessoa){
        $inicio = date('Y-m-d H:i:s');
        if($this->conectar(EnumURL::wsPessoa)){
            try{
                $dados = $this->clienteWS->CadastrarPessoa(array('pessoaDto'=>$pessoa));
                $fim = date('Y-m-d H:i:s');
                $this->registrarChamada('CadastrarPessoa', $inicio, $fim,$dados,$pessoa);
                return $dados;
            }catch(\Exception $e){
                $this->setErro($e->getMessage(), 'CadastrarPessoa', $inicio, $fim, $pessoa);
            }
        }
        return false;
    }

    public function cadastrarResponsavelFinanceiro($pessoa){
        $inicio = date('Y-m-d H:i:s');
        if($this->conectar(EnumURL::wsPessoa)){
            try{
                $dados = $this->clienteWS->CadastrarResponsavelFinanceiro(array('responsavelFinanceiroDto'=>$pessoa));
                $fim = date('Y-m-d H:i:s');
                $this->registrarChamada('CadastrarResponsavelFinanceiro', $inicio, $fim,$dados,$pessoa);
                return $dados;
            }catch(\Exception $e){
                $this->setErro($e->getMessage(), 'CadastrarResponsavelFinanceiro', $inicio, $fim, $pessoa);
            }
        }
        return false;
    }

}
