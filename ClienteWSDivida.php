<?php
namespace Rubeus\IntegracaoLyceum;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Servicos\String\Caracter;

class ClienteWSDivida extends ClienteWS{

    public function criarDivida($dadosDivida){
        $inicio = date('Y-m-d H:i:s');
        if($this->conectar(EnumURL::wsDivida)){
            try{
                $dados = $this->clienteWS->CadastrarDivida(["FiltroDividaDto" => $dadosDivida]);
                $fim = date('Y-m-d H:i:s');
                $this->registrarChamada('CadastrarDivida', $inicio, $fim, $dados, $dadosDivida);
                return $dados->DividaDto;
            }catch(\Exception $e){
                $this->setErro($e->getMessage(), 'CadastrarDivida', $inicio, $fim, $dadosDivida);
            }
        }
        return false;
    }

}
