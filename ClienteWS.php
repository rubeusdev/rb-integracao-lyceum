<?php
namespace Rubeus\IntegracaoLyceum;
use Rubeus\ContenerDependencia\Conteiner;

class ClienteWS {
    protected $clienteWS = false;
    protected $wsAnterior = false;
    protected $erro = false;

    private $usuario;
    private $senha;
    private $dirBase;

    public function __construct(){
        if(defined('DIR_FILE_INSTITUICAO')){
            $this->dirBase = DIR_BASE.'/file/'.DIR_FILE_INSTITUICAO.'/';
        }else{
            $this->dirBase = DIR_BASE.'/file/';
        }
        if(!file_exists($this->dirBase.'/logchamadawslyceum/')){
            mkdir($this->dirBase.'/logchamadawslyceum/');
        }
        if(!file_exists($this->dirBase.'/logchamadawslyceum/'.$metodo.'/')){
            mkdir($this->dirBase.'/logchamadawslyceum/'.$metodo.'/');
        }
    }

    protected function conectar($ws){
        $this->erro = false;
        if($this->clienteWS && $this->wsAnterior == $ws){
            return true;
        }
        $this->wsAnterior = $ws;
        $url = ENDERECO_BASE_INTEGRACAO_LYCEUM.'/'.$ws;
        try{
            $this->clienteWS = new \SoapClient($url);
        }catch(\Exception $e){
            $this->setErro($e->getMessage(), 'Conectar', $url);
            return false;
        }
        return true;
    }

    public function getErro(){
        return $this->erro;
    }

    protected function setErro($erro, $metodo, $inicio=false, $fim=false, $dadosEnvio=false){
        $this->erro = $erro;
        Conteiner::get('RegistrarChamadaLyceum')->registrarErro($erro, $metodo, $this->wsAnterior, $inicio, $fim, json_encode($dadosEnvio));
    }

    protected function registrarChamada($metodo, $inicio, $fim, $dados, $dadosEnvio){
        $enderecoResponse = $this->dirBase.'/logchamadawslyceum/'.$metodo.'/response_'.date('Y_m_d_H_i_s_').rand(100,1300).'.txt';

        file_put_contents($enderecoResponse, json_encode($dados));

        Conteiner::get('RegistrarChamadaLyceum')->registrar($enderecoResponse, $metodo, $this->wsAnterior, $inicio, $fim, json_encode($dadosEnvio));
    }

}
