<?php
namespace Rubeus\IntegracaoLyceum;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Servicos\String\Caracter;

class ClienteWSCobranca extends ClienteWS{

    public function criarCobranca($dadosCobranca){
        $inicio = date('Y-m-d H:i:s');
        if($this->conectar(EnumURL::wsCobranca)){
            try{
                $dados = $this->clienteWS->GerarCobrancasAluno(["infoGeracaoCobrancasAlunoDto" => $dadosCobranca]);
                $fim = date('Y-m-d H:i:s');
                $this->registrarChamada('GerarCobrancasAluno', $inicio, $fim, $dados,$dadosCobranca);
                return $dados->listaCobrancasDto->ListaCobrancasDto;
            }catch(\Exception $e){
                $this->setErro($e->getMessage(), 'GerarCobrancasAluno', $inicio, $fim,$dadosCobranca);
            }
        }
        return false;
    }

    public function faturarCobranca($dadosCobranca){
        $inicio = date('Y-m-d H:i:s');
        if($this->conectar(EnumURL::wsCobranca)){
            try{
                $dados = $this->clienteWS->FaturarCobrancas(["ContaCobrancaDto" => $dadosCobranca]);
                $fim = date('Y-m-d H:i:s');
                $this->registrarChamada('FaturarCobrancas', $inicio, $fim, $dados, $dadosCobranca);
                return $dados;
            }catch(\Exception $e){
                $this->setErro($e->getMessage(), 'FaturarCobrancas', $inicio, $fim,$dadosCobranca);
            }
        }
        return false;
    }

}
