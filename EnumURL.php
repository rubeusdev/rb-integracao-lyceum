<?php
namespace Rubeus\IntegracaoLyceum;

class EnumURL {
    const wsPessoa = '/Unijorge/ServicoIntegracao?wsdl';
    const wsAluno = '/ServicoIngresso?wsdl';
    const wsFinanceiro = '/ServicoFinanceiro?wsdl';
    const wsDivida = '/ServicoDivida?wsdl';
    const wsCobranca = '/ServicoCobranca?wsdl';
    const wsLocal = '/ServicoLocal?wsdl';
}
