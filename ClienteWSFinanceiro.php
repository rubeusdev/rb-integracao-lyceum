<?php
namespace Rubeus\IntegracaoLyceum;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Servicos\String\Caracter;

class ClienteWSFinanceiro extends ClienteWS{

    public function criarPlanoPagamento($dadosPlanoPagamento){
        $inicio = date('Y-m-d H:i:s');
        if($this->conectar(EnumURL::wsFinanceiro)){
            try{
                $dados = $this->clienteWS->CadastrarPlanosPagamentoPeriodo(["planoPagamentoPeriodoAlunoDto" => $dadosPlanoPagamento]);
                $fim = date('Y-m-d H:i:s');
                $this->registrarChamada('CadastrarPlanosPagamentoPeriodo', $inicio, $fim, $dados,$dadosPlanoPagamento);
                return $dados;
            }catch(\Exception $e){
                $this->setErro($e->getMessage(), 'CadastrarPlanosPagamentoPeriodo', $inicio, $fim, $dadosPlanoPagamento);
            }
        }
        return false;
    }

    public function baixarPagamentoCobranca($dadosBaixa){
        $inicio = date('Y-m-d H:i:s');
        if($this->conectar(EnumURL::wsFinanceiro)){
            try{
                $dados = $this->clienteWS->BaixaPagamentoCobranca($dadosBaixa);
                $fim = date('Y-m-d H:i:s');
                $this->registrarChamada('BaixaPagamentoCobranca', $inicio, $fim, $dados, $dadosBaixa);
                return $dados;
            }catch(\Exception $e){
                $this->setErro($e->getMessage(), 'BaixaPagamentoCobranca', $inicio, $fim, $dadosBaixa);
            }
        }
        return false;
    }

}
