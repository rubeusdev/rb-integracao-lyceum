<?php
namespace Rubeus\IntegracaoLyceum;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Servicos\String\Caracter;

class ClienteWSLocal extends ClienteWS{

    public function codigoCidade($dadosConsulta){
        $inicio = date('Y-m-d H:i:s');
        if($this->conectar(EnumURL::wsLocal)){
            try{
                $dados = $this->clienteWS->ListarMunicipios(["municipioDto" => $dadosConsulta]);
                $fim = date('Y-m-d H:i:s');
                $this->registrarChamada('ListarMunicipios', $inicio, $fim, $dados,$dadosConsulta);
                $resultadoConsulta =  $dados->listaMunicipiosDto->listaMunicipiosDto;
                if(!is_array($resultadoConsulta)){
                    $resultadoConsulta= [$resultadoConsulta];
                }               
                for($i=0;$i<count($resultadoConsulta);$i++){
                    if(strtoupper(Caracter::removerCarctAcen($dadosConsulta['nome'])) == strtoupper(Caracter::removerCarctAcen($resultadoConsulta[$i]->nome))){
                        return $resultadoConsulta[$i]->codigo;
                    }
                }
            }catch(\Exception $e){
                $this->setErro($e->getMessage(), 'ListarMunicipios', $inicio, $fim, $dadosConsulta);
            }
        }
        return false;
    }

}
