<?php
namespace Rubeus\IntegracaoLyceum;
use Rubeus\ContenerDependencia\Conteiner;

class ClienteWSAluno extends ClienteWS{

    public function cadastrarAluno($aluno){
        $inicio = date('Y-m-d H:i:s');
        if($this->conectar(EnumURL::wsAluno)){
            try{
                $dados = $this->clienteWS->CadastrarIngressarAluno(array('alunoDto'=>$aluno));
                $fim = date('Y-m-d H:i:s');
                $this->registrarChamada('CadastrarIngressarAluno', $inicio, $fim,$dados, $aluno);
                return $dados->alunoRetornoDto;
            }catch(\Exception $e){
                if($e->getMessage() != 'Já existe aluno ativo desta pessoa cadastrada nesse curso.'){
                    $this->setErro($e->getMessage(), 'CadastrarIngressarAluno', $inicio, $fim, $aluno);
                }
            }
        }
        return false;
    }

}
